<?php

/* sections.html.twig */
class __TwigTemplate_d3733f3bab0ca8f6ecae9be89ece7d0d7f82543b1527c24d73f029901d8091d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("partials/base.html.twig", "sections.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "partials/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
    <div class=\"columns\">

        <div id=\"sections-content\" class=\"column col-9 col-md-12\">

            ";
        // line 9
        if (( !$this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", array()), "hide_page_title", array()) &&  !$this->getAttribute($this->getAttribute(($context["grav"] ?? null), "uri", array()), "param", array(0 => "hidepagetitle"), "method"))) {
            // line 10
            echo "                <h3 class=\"sections-page-title-template\">";
            echo $this->getAttribute(($context["page"] ?? null), "title", array());
            echo "</h3>
            ";
        }
        // line 12
        echo "
            ";
        // line 13
        echo $this->getAttribute(($context["page"] ?? null), "content", array());
        echo "

            <p class=\"prev-next\">
                ";
        // line 16
        $context["siblings"] = $this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "children", array(), "method");
        // line 17
        echo "                ";
        $context["children"] = $this->getAttribute(($context["page"] ?? null), "children", array(), "method");
        // line 18
        echo "
                ";
        // line 19
        if ( !($this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "slug", array()) == "pages")) {
            // line 20
            echo "
            <p>

                ";
            // line 23
            if ( !$this->getAttribute(($context["siblings"] ?? null), "isFirst", array(0 => $this->getAttribute(($context["page"] ?? null), "path", array())), "method")) {
                // line 24
                echo "                    ";
                if (($this->getAttribute($this->getAttribute(($context["page"] ?? null), "children", array()), "count", array()) == 0)) {
                    // line 25
                    echo "                        <a class=\"btn btn-primary\" href=\"";
                    echo $this->getAttribute($this->getAttribute(($context["siblings"] ?? null), "nextSibling", array(0 => $this->getAttribute(($context["page"] ?? null), "path", array())), "method"), "url", array());
                    echo "\">
                            <i class=\"fa fa-chevron-left\"></i>
                            ";
                    // line 27
                    echo $this->getAttribute($this->getAttribute(($context["siblings"] ?? null), "nextSibling", array(0 => $this->getAttribute(($context["page"] ?? null), "path", array())), "method"), "title", array());
                    echo "</a>
                    ";
                } else {
                    // line 29
                    echo "                        <a class=\"btn btn-primary\" href=\"";
                    echo $this->getAttribute($this->getAttribute(($context["siblings"] ?? null), "nextSibling", array(0 => $this->getAttribute(($context["page"] ?? null), "path", array())), "method"), "url", array());
                    echo "\">
                            <i class=\"fa fa-chevron-left\"></i>
                            ";
                    // line 31
                    echo $this->getAttribute($this->getAttribute(($context["siblings"] ?? null), "nextSibling", array(0 => $this->getAttribute(($context["page"] ?? null), "path", array())), "method"), "title", array());
                    echo "</a>
                    ";
                }
                // line 33
                echo "                ";
            } else {
                // line 34
                echo "                    ";
                if ( !($this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "parent", array()), "slug", array()) == "pages")) {
                    // line 35
                    echo "                        <a class=\"btn btn-primary\" href=\"";
                    echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "url", array());
                    echo "\">
                            <i class=\"fa fa-chevron-left\"></i>
                            ";
                    // line 37
                    echo $this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "title", array());
                    echo "</a>
                    ";
                }
                // line 39
                echo "                ";
            }
            // line 40
            echo "
                ";
            // line 41
            if ( !$this->getAttribute(($context["siblings"] ?? null), "isLast", array(0 => $this->getAttribute(($context["page"] ?? null), "path", array())), "method")) {
                // line 42
                echo "                    ";
                if (($this->getAttribute($this->getAttribute(($context["page"] ?? null), "children", array()), "count", array()) == 0)) {
                    // line 43
                    echo "                        <a class=\"btn btn-primary\" href=\"";
                    echo $this->getAttribute($this->getAttribute(($context["siblings"] ?? null), "prevSibling", array(0 => $this->getAttribute(($context["page"] ?? null), "path", array())), "method"), "url", array());
                    echo "\">";
                    echo $this->getAttribute($this->getAttribute(($context["siblings"] ?? null), "prevSibling", array(0 => $this->getAttribute(($context["page"] ?? null), "path", array())), "method"), "title", array());
                    echo "
                            <i class=\"fa fa-chevron-right\"></i>
                        </a>
                    ";
                } else {
                    // line 47
                    echo "                        <a class=\"btn btn-primary\" href=\"";
                    echo $this->getAttribute(twig_first($this->env, $this->getAttribute(($context["page"] ?? null), "children", array())), "url", array());
                    echo "\">";
                    echo $this->getAttribute(twig_first($this->env, $this->getAttribute(($context["page"] ?? null), "children", array())), "title", array());
                    echo "
                            <i class=\"fa fa-chevron-right\"></i>
                        </a>
                    ";
                }
                // line 51
                echo "                ";
            } else {
                // line 52
                echo "                    ";
                if ( !($this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "parent", array()), "slug", array()) == "pages")) {
                    // line 53
                    echo "                        ";
                    $context["siblings"] = $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "parent", array()), "children", array(), "method");
                    // line 54
                    echo "                        ";
                    if ( !$this->getAttribute(($context["siblings"] ?? null), "isLast", array(0 => $this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "path", array())), "method")) {
                        // line 55
                        echo "                            <a class=\"btn btn-primary\"
                               href=\"";
                        // line 56
                        echo $this->getAttribute($this->getAttribute(($context["siblings"] ?? null), "prevSibling", array(0 => $this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "path", array())), "method"), "url", array());
                        echo "\">";
                        echo $this->getAttribute($this->getAttribute(($context["siblings"] ?? null), "prevSibling", array(0 => $this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "path", array())), "method"), "title", array());
                        echo "
                                <i class=\"fa fa-chevron-right\"></i>
                            </a>
                        ";
                    }
                    // line 60
                    echo "                    ";
                } else {
                    // line 61
                    echo "                        <a class=\"btn btn-primary\" href=\"";
                    echo $this->getAttribute(twig_first($this->env, $this->getAttribute(($context["page"] ?? null), "children", array())), "url", array());
                    echo "\">";
                    echo $this->getAttribute(twig_first($this->env, $this->getAttribute(($context["page"] ?? null), "children", array())), "title", array());
                    echo "
                            <i class=\"fa fa-chevron-right\"></i>
                        </a>
                    ";
                }
                // line 65
                echo "                ";
            }
            // line 66
            echo "
            </p>

            ";
        }
        // line 70
        echo "
        </div>

        <div id=\"sections-nav\" class=\"column col-3 col-md-12\">

            <ul class=\"nav\">

                ";
        // line 77
        $context["collection"] = $this->getAttribute(($context["page"] ?? null), "collection", array(), "method");
        // line 78
        echo "
                ";
        // line 79
        if (twig_test_empty(($context["collection"] ?? null))) {
            // line 80
            echo "                    ";
            $context["collection"] = $this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "collection", array(), "method");
            // line 81
            echo "                    ";
            if (twig_test_empty(($context["collection"] ?? null))) {
                // line 82
                echo "                        ";
                $context["collection"] = $this->getAttribute($this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "parent", array()), "collection", array(), "method");
                // line 83
                echo "                    ";
            }
            // line 84
            echo "                ";
        }
        // line 85
        echo "
                ";
        // line 86
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["collection"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 87
            echo "
                    ";
            // line 88
            $context["current_parent"] = (($this->getAttribute($context["p"], "active", array())) ? ("active") : (""));
            // line 91
            echo "                    <li class=\"nav-item  ";
            echo ($context["current_parent"] ?? null);
            echo "\">
                        <a href=\"";
            // line 92
            echo $this->getAttribute($context["p"], "url", array());
            echo "\">";
            echo $this->getAttribute($context["p"], "title", array());
            echo "</a>
                    </li>

                    ";
            // line 95
            if (($this->getAttribute($this->getAttribute($context["p"], "children", array()), "count", array()) != 0)) {
                // line 96
                echo "
                        ";
                // line 97
                if (($this->getAttribute($context["p"], "active", array()) || ($this->getAttribute($context["p"], "slug", array()) == $this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", array()), "slug", array())))) {
                    // line 98
                    echo "                            <ul class=\"nav\">
                                ";
                    // line 99
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["p"], "children", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 100
                        echo "                                    ";
                        $context["current_child"] = (($this->getAttribute($context["child"], "active", array())) ? ("active") : (""));
                        // line 103
                        echo "                                    <li class=\"nav-item ";
                        echo ($context["current_child"] ?? null);
                        echo "\">
                                        <a href=\"";
                        // line 104
                        echo $this->getAttribute($context["child"], "url", array());
                        echo "\">";
                        echo $this->getAttribute($context["child"], "title", array());
                        echo "</a>
                                    </li>
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 107
                    echo "                            </ul>
                        ";
                }
                // line 109
                echo "
                    ";
            }
            // line 111
            echo "
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 113
        echo "
            </ul>

        </div>

    </div>

";
    }

    public function getTemplateName()
    {
        return "sections.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  295 => 113,  288 => 111,  284 => 109,  280 => 107,  269 => 104,  264 => 103,  261 => 100,  257 => 99,  254 => 98,  252 => 97,  249 => 96,  247 => 95,  239 => 92,  234 => 91,  232 => 88,  229 => 87,  225 => 86,  222 => 85,  219 => 84,  216 => 83,  213 => 82,  210 => 81,  207 => 80,  205 => 79,  202 => 78,  200 => 77,  191 => 70,  185 => 66,  182 => 65,  172 => 61,  169 => 60,  160 => 56,  157 => 55,  154 => 54,  151 => 53,  148 => 52,  145 => 51,  135 => 47,  125 => 43,  122 => 42,  120 => 41,  117 => 40,  114 => 39,  109 => 37,  103 => 35,  100 => 34,  97 => 33,  92 => 31,  86 => 29,  81 => 27,  75 => 25,  72 => 24,  70 => 23,  65 => 20,  63 => 19,  60 => 18,  57 => 17,  55 => 16,  49 => 13,  46 => 12,  40 => 10,  38 => 9,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'partials/base.html.twig' %}

{% block content %}

    <div class=\"columns\">

        <div id=\"sections-content\" class=\"column col-9 col-md-12\">

            {% if not(page.header.hide_page_title)and not(grav.uri.param('hidepagetitle')) %}
                <h3 class=\"sections-page-title-template\">{{ page.title }}</h3>
            {% endif %}

            {{ page.content }}

            <p class=\"prev-next\">
                {% set siblings = page.parent.children() %}
                {% set children = page.children() %}

                {% if not(page.parent.slug == 'pages') %}

            <p>

                {% if not siblings.isFirst(page.path) %}
                    {% if page.children.count == 0 %}
                        <a class=\"btn btn-primary\" href=\"{{ siblings.nextSibling(page.path).url }}\">
                            <i class=\"fa fa-chevron-left\"></i>
                            {{ siblings.nextSibling(page.path).title }}</a>
                    {% else %}
                        <a class=\"btn btn-primary\" href=\"{{ siblings.nextSibling(page.path).url }}\">
                            <i class=\"fa fa-chevron-left\"></i>
                            {{ siblings.nextSibling(page.path).title }}</a>
                    {% endif %}
                {% else %}
                    {% if not(page.parent.parent.slug == 'pages') %}
                        <a class=\"btn btn-primary\" href=\"{{ page.parent.url }}\">
                            <i class=\"fa fa-chevron-left\"></i>
                            {{ page.parent.title }}</a>
                    {% endif %}
                {% endif %}

                {% if not siblings.isLast(page.path) %}
                    {% if page.children.count == 0 %}
                        <a class=\"btn btn-primary\" href=\"{{ siblings.prevSibling(page.path).url }}\">{{ siblings.prevSibling(page.path).title }}
                            <i class=\"fa fa-chevron-right\"></i>
                        </a>
                    {% else %}
                        <a class=\"btn btn-primary\" href=\"{{ page.children|first.url }}\">{{ page.children|first.title }}
                            <i class=\"fa fa-chevron-right\"></i>
                        </a>
                    {% endif %}
                {% else %}
                    {% if not(page.parent.parent.slug == 'pages') %}
                        {% set siblings = page.parent.parent.children() %}
                        {% if not siblings.isLast(page.parent.path) %}
                            <a class=\"btn btn-primary\"
                               href=\"{{ siblings.prevSibling(page.parent.path).url }}\">{{ siblings.prevSibling(page.parent.path).title }}
                                <i class=\"fa fa-chevron-right\"></i>
                            </a>
                        {% endif %}
                    {% else %}
                        <a class=\"btn btn-primary\" href=\"{{ page.children|first.url }}\">{{ page.children|first.title }}
                            <i class=\"fa fa-chevron-right\"></i>
                        </a>
                    {% endif %}
                {% endif %}

            </p>

            {% endif %}

        </div>

        <div id=\"sections-nav\" class=\"column col-3 col-md-12\">

            <ul class=\"nav\">

                {% set collection = page.collection() %}

                {% if collection is empty %}
                    {% set collection = page.parent.collection() %}
                    {% if collection is empty %}
                        {% set collection = page.parent.parent.collection() %}
                    {% endif %}
                {% endif %}

                {% for p in collection %}

                    {% set current_parent = p.active
                        ? 'active'
                        : '' %}
                    <li class=\"nav-item  {{ current_parent }}\">
                        <a href=\"{{ p.url }}\">{{ p.title }}</a>
                    </li>

                    {% if p.children.count != 0 %}

                        {% if p.active or(p.slug == page.parent.slug) %}
                            <ul class=\"nav\">
                                {% for child in p.children %}
                                    {% set current_child = child.active
                                        ? 'active'
                                        : '' %}
                                    <li class=\"nav-item {{ current_child }}\">
                                        <a href=\"{{ child.url }}\">{{ child.title }}</a>
                                    </li>
                                {% endfor %}
                            </ul>
                        {% endif %}

                    {% endif %}

                {% endfor %}

            </ul>

        </div>

    </div>

{% endblock %}
", "sections.html.twig", "C:\\xampp\\htdocs\\grav-blog\\user\\themes\\quark-open-publishing\\templates\\sections.html.twig");
    }
}
