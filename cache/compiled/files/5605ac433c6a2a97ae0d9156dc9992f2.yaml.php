<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/grav-blog/user/config/plugins/shortcode-core.yaml',
    'modified' => 1546874808,
    'data' => [
        'enabled' => true,
        'active' => true,
        'active_admin' => true,
        'parser' => 'regex',
        'fontawesome' => [
            'load' => false,
            'url' => '//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css'
        ]
    ]
];
