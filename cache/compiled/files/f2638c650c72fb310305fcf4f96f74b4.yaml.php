<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://problems/problems.yaml',
    'modified' => 1546874874,
    'data' => [
        'enabled' => true,
        'built_in_css' => true
    ]
];
