<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/grav-blog/user/plugins/image-captions/image-captions.yaml',
    'modified' => 1546874866,
    'data' => [
        'enabled' => true,
        'built_in_css' => true,
        'entire_page' => false,
        'scope' => 'img.caption',
        'figure_class' => 'image-caption',
        'figcaption_class' => NULL
    ]
];
