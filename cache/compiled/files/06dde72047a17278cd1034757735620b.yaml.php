<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://shortcode-ui/shortcode-ui.yaml',
    'modified' => 1546874886,
    'data' => [
        'enabled' => true,
        'theme' => [
            'tabs' => 'default'
        ]
    ]
];
