---
title: Blog
media_order: fondo1.jpg
hide_git_sync_repo_link: false
body_classes: 'header-dark header-transparent'
hero_classes: 'text-light title-h1h2 overlay-dark-gradient hero-large parallax'
blog_url: /blog
show_sidebar: false
show_breadcrumbs: true
show_pagination: true
content:
    items:
        - '@self.children'
    limit: 6
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
bricklayer_layout: true
display_post_summary:
    enabled: false
sitemap:
    changefreq: monthly
feed:
    description: 'Sample Blog Description'
    limit: 10
pagination: true
---

# Nuevo **Blog** con Grav CMS
## Examen final graficación y animación
