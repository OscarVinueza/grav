---
title: 'Examen de diagnostico'
media_order: Test-WP.jpg
date: '23:26 14-01-2019'
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: false
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
---

<p>En el quiz, primero entramos en la p&aacute;gina web: https://w3schools.com/quiztest/ en la cual se pueden seleccionar diferentes tipos de test, nosotros accedimos al test de html, javascript y css para poner a prueba nuestros conocimientos previos antes de aprender en la materia el uso de los mismos, el test no fue nada complicado y se preguntaban conceptos bastante b&aacute;sicos a cerca del uso de estas herramientas.</p>