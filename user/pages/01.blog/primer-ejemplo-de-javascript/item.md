---
title: 'Primer ejemplo de Javascript'
media_order: js-logo.png
date: '22:57 14-01-2019'
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: false
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
---

<p>Para este ejemplo primero, entramos a la p&aacute;gina de: https://www.w3schools.com en la cual podemos acceder a cursos de javascript, obtener un certificado o realizar una prueba de diagn&oacute;stico como la que hicimos en blogs anteriores, un ejemplo de esto es el que nos plantean a continuaci&oacute;n:</p> <p>&nbsp;</p> <h2>What Can JavaScript Do?</h2> <p id="demo">JavaScript can change HTML content.</p> <button type="button" onclick='document.getElementById("demo").innerHTML = "Hello JavaScript!"'>Click Me!</button> <p>&nbsp;</p> <p><img alt="Resultado de imagen para javascript" src="https://fututel.com/images/tutorials/objetos-de-javascript.jpg" style="width:100%;margin-top:50px"/></p> <p>&nbsp;</p>