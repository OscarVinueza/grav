---
title: 'Blog con Docker'
media_order: 8.jpg
date: '23:27 14-01-2019'
hide_git_sync_repo_link: false
blog_url: /blog
show_sidebar: false
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
---

En esta ocasión, realizamos un blog con el framework yii2 utilizando la plantilla avanzada para poder administrar el contenido del blog y los comentarios del mismo, para esto tambien fué necesario utilizar un repositorio bajo git, en el cual pudimos poner nuestro código para luego poder bajarlo en el servidor y asi mostrarlo a todo el mundo a través del servidor web, de este modo trajimos a ustedes este blog, espero que les guste !!
