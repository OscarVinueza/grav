---
title: 'Creación de servidor apache con Docker'
media_order: docker.png
date: '17:34 06/27/2017'
taxonomy:
    category:
        - blog
hide_git_sync_repo_link: false
hero_classes: 'text-dark title-h1h2 overlay-light hero-large parallax'
hero_image: docker.png
blog_url: /blog
show_sidebar: false
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
subtitle: 'finding beauty in structure'
---

Creación de un servidor web con docker, para lograr esto se debió descargar el docker en nuestro servidor, luego descargar una imagen de ubuntu con php, y apache apropiada para que haga de servidor, descargar una imagen de mysql y otra de phpmyadmin como gestor de base de datos, posteriormente se enlazó el contenedor de mysql con el de phpmyadmin y se levanto el servidor apache, luego una vez dentro del contenedor del servidor se procedió a instalar las dependencias necesarias y configurar un archivo index.php que nos diera nuestro hola mundo, y listo !! asi fué como creamos nuestro server
